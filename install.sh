#!/bin/bash
echo "Installing Poorna Extended malayalam-in..."
#taking backup
mkdir -p /usr/share/doc/Poorna-ml
cp /usr/share/X11/xkb/symbols/in /usr/share/doc/Poorna-ml/in.orig
cp /usr/share/X11/xkb/rules/evdev.xml /usr/share/doc/Poorna-ml/evdev.xml.orig
#copy files to location
cp in /usr/share/X11/xkb/symbols/
cp evdev.xml /usr/share/X11/xkb/rules/
sleep 1
echo "Done"
