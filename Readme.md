This is an extended inscript keyboard which includes all malayalam charecters.
# install
run 
`sudo ./install.sh`

# Use
After installation it can be used by adding India Malayalam Poorna Extended layout in gnome-keyboard-properties or in kde keyboard settings.

or use the command to activate the keyboard\
`setxkbmap -model pc105 -layout us,in -variant ,mal_poorna  -option grp:alt_shift_toggle`

The layout is a four layer one, that is a single key maps to a maximum of 4 characters. For example,

in case of 'd',\
'd' gives ്\
'Shift+d' gives അ\
'Right Alt+d' gives ഽ\
'Right Alt+Shift+d' gives ഁ

# Layout Image
![Image](poorna.png)


